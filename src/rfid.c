/*This file is part of hardware.

    hardware is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    hardware is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with hardware.  If not, see <http://www.gnu.org/licenses/>. */

#include "rfid.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

card_t *head = NULL;
